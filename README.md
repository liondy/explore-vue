# vue-explore

Berikut adalah laporan hasil eksplorasi dengan membuat project explorasi vue dengan menggunakan `typescript` \
Eksplorasi dibuat berdasarkan [referensi](#reference) \
Vue cli yang digunakan `v4.5.13` \
Fitur yang digunakan:
1. Vue versi 3
2. Babel --> default
3. Typescript --> sesuai kebutuhan
4. Linter / Formatter --> default
5. Sisanya seperti PWA Support, Router, Vuex belum digunakan

Detil fitur dapat dilihat pada gambar dibawah ini: \
![Pemilihan Fitur](screenshots/features.png)

Konfigurasi instalasi vue:
1. Fitur manual
2. Class-component syntax: Ya, agar dapat melakukan pemrograman secara OOP dan lebih mudah di maintain
3. Babel Alongside Typescript: Ya, belum mengerti fungsinya
4. Formatter config: Airbnb, untuk aturan penulisan kode progam (briefing dari Daniel C)
5. Additional Lint Features: Lint on save, agar apabila ada kesalahan penulisan dapat langsung diperbaiki saat melakukan save
6. Penyimpanan Babel config: dedicated config files --> ada di .eslintrc.js

Proses pemilihan konfigurasi dapat dilihat pada gambar dibawah ini:\
![Konfigurasi Vue](screenshots/conf.png)

## Reference
- Vue JS [Documentation](https://vuejs.org/v2/guide/)
- Freecodecamp - [Learn Typescript - full](https://www.youtube.com/watch?v=gp5H0Vw39yw)
- Youtube The Net Ninja - [Vue JS 3 Tutorial for Beginners](https://www.youtube.com/watch?v=YrxBCBibVo0)
- Youtube The Net Ninja - [Vue JS 3 with Typescript Tutorial](https://www.youtube.com/watch?v=JfI5PISLr9w)